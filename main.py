# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

def check_input():
    try:
        lines = int(input("please enter height of the square \n"))
        if lines < 0:
            print("Input is negative...")
        else:
            print_cube(lines)

    except ValueError:
        print("Input is not an integer...")


def print_cube(lines):
    if lines == 0:
        print("  +-----+")
        print(" /     /")
        print("+-----+  ")
    else:
        print("  +-----+")
        print(" /     /|")

        print("+-----+ |")
        for x in range(lines):

            if x == lines - 2:
                print("|     | +")
            elif x == lines - 1:
                print("|     |/")
            else:
                print("|     | |")
        print("+-----+")


if __name__ == "__main__":
    while True:
        check_input()
